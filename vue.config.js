const publicPath = process.env.NODE_ENV === 'development' ? '/' : '/dftgame'

module.exports = {
  publicPath: publicPath,
  devServer: {
    proxy: 'http://119.3.14.11:9090'
  }
}
