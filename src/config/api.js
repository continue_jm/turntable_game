/**
 * @author jinming
 * @date 2020/9/6 16:10
 * @desc 接口路径地址
 */

const api = {
  gameConfiguration: 'app/game/configuration',
  lotteryResult: 'app/game/lotteryResult',
  getPersonList: 'app/game/message'
}

export default api
