/**
 * @author jinming
 * @date 2020/9/6 17:43
 * @desc 封装公共方法
 */
class PublicUtil {
  // 获取url参数
  static getQueryVariable (variable) {
    const query = location.search.substring(1)
    const vars = query.split('&')
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=')
      if (pair[0] === variable) {
        return pair[1]
      }
    }
    return ''
  }
}

export default PublicUtil
