/**
 * @author jinming
 * @date 2020/9/6 16:12
 * @desc 封装post，get请求
 */
import axios from 'axios'
import api from '../config/api'
import { Toast } from 'vant'

// pro
// const baseURL = process.env.NODE_ENV === 'development' ? '/main/dftclient' : 'https://znyx.dafaland.com/dftclient'

// dev
const baseURL = process.env.NODE_ENV === 'development' ? '/main/dftclient' : 'http://119.3.14.11:9090/dftclient'

class AxiosUtil {
  static post (params) {
    return new Promise((resolve, reject) => {
      axios({
        baseURL,
        method: 'post',
        url: api[params.url],
        data: params.data,
        headers: {
          AuthorizationToken: sessionStorage.getItem('AuthorizationToken'),
          'Content-Type': 'application/json'
        }
      })
        .then(response => {
          if (response.status !== 200) {
            reject(response.data)
            return
          }
          resolve(response.data)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          Toast.clear()
        })
    })
  }

  static get (params) {
    return new Promise((resolve, reject) => {
      axios({
        baseURL,
        method: 'get',
        url: api[params.url],
        params: params.data,
        headers: {
          AuthorizationToken: sessionStorage.getItem('AuthorizationToken'),
          'Content-Type': 'application/json'
        }
      })
        .then(response => {
          if (response.status !== 200) {
            reject(response.data)
            return
          }
          resolve(response.data)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          Toast.clear()
        })
    })
  }
}

export default AxiosUtil
